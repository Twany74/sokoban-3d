using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Move : MonoBehaviour
{
    public bool IsMoving { get; protected set; }
    public delegate void MoveAction();
    public static event MoveAction OnMove;

    public virtual void MoveTo(Direction direction)
    {

    }
    
    public static void HasMoved()
    {
        if (OnMove != null)
        {
            OnMove();
        }
    }
}
