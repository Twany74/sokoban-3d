using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TopViewManager : MonoBehaviour
{
    public UnityEvent OnTopViewEnter;
    public UnityEvent OnTopViewLeave;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnTopViewEnter.Invoke();
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            OnTopViewLeave.Invoke();
        }
    }
}
