using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translate : Move
{
    [SerializeField] private float _moveSpeedInMeterPerSecond = 3.0f;
    private Entity _entity;
    public bool crateHasMoved { get; private set; }

    void Awake()
    {
        _entity = GetComponent<Entity>();
    }

    public override void MoveTo(Direction direction)
    {
        crateHasMoved = false;
        if (IsMoving == false)
        {
            if (GameBoard.Instance.CanMove(_entity, direction))
            {
                GameBoard.Instance.Unregister(_entity);
                StartCoroutine(TranslateEntity(DirectionHelper.ToVector3(direction), _moveSpeedInMeterPerSecond));
            }
        }
    }

    public IEnumerator TranslateEntity(Vector3 direction, float _moveSpeed)
    {
        crateHasMoved = true;
        float remainingDeplacement = 1.0f;
        IsMoving = true;
        while (remainingDeplacement > 0)
        {
            float translation = Mathf.Min(Time.deltaTime * _moveSpeed, remainingDeplacement);
            _entity.transform.Translate(direction * translation);
            remainingDeplacement -= translation;
            yield return null;
        }
        IsMoving = false;
        GameBoard.Instance.Register(_entity);
    }
}
