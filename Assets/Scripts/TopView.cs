using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopView : MonoBehaviour
{
    private Vector3 _velocity = Vector3.zero;
    private Vector3 _targetPosition = new Vector3(5.0f, 11.0f, 5.0f);
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(90.0f, 0, 0), Time.deltaTime * 2.0f);
        transform.position = Vector3.SmoothDamp(transform.position, _targetPosition, ref _velocity, 2.0f);
    }
}
