using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private GameObject _player;

    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private Vector3 offset = new Vector3(0, 4.5f, -3.0f);
    [SerializeField] private float smoothTimeInSeconds = 0.5f;
    private Vector3 velocity = Vector3.zero;

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPosition = new Vector3(_player.transform.position.x, 0, _player.transform.position.z) + offset;
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTimeInSeconds);
    }
}
