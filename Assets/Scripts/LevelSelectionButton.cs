using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelSelectionButton : MonoBehaviour
{
    public int lvl { get; private set; }
    // Start is called before the first frame update
    void Update()
    {
        Button button = this.GetComponent<Button>();
        TMP_Text buttonText = button.GetComponentInChildren<TMP_Text>();
        this.lvl = Int32.Parse(buttonText.text);
        button.interactable = LevelManager.Instance.IsLevelAvailable[this.lvl - 1];
    }

    public void LoadSelectedLevel()
    {
        LevelManager.LoadLevel(this.lvl);
    }
}
