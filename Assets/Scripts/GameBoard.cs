using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameBoard : MonoBehaviour
{
    private Vector3 _playerSpawn = new Vector3(1.5f, 0.5f, 8.5f);
    [SerializeField] private int _boardMaxWidth = 10;
    [SerializeField] private int _boardMaxHeight = 10;
    public static GameBoard Instance;
    private static Grid _grid;
    private Cell[,] _cells;
    private Vector3Int _nextCoordinate;
    private List<Entity> _entities;
    private int _numberOfTargets = 0;
    public UnityEvent OnLevelFinished;

    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("can't have 2 Instances of GameBoard");
            Destroy(gameObject);
        }
        else
        {
            Instance = this;

            _entities = new List<Entity>();
            _grid = GetComponent<Grid>();

            _cells = new Cell[_boardMaxWidth, _boardMaxHeight];
            for (int i = 0; i < _boardMaxWidth; ++i)
                for (int j = 0; j < _boardMaxHeight; j++)
                {
                    _cells[i, j] = new Cell();
                }
        }
        Move.OnMove += CheckForWin;
        DontDestroyOnLoad(this);
    }

    public void RestorePlayerLocation()
    {
        foreach (Entity entity in _entities)
        {
            if (entity.GetComponent<PlayerControls>() != null)
            {
                GameBoard.Instance.Unregister(entity);
                entity.transform.position = _playerSpawn;
                GameBoard.Instance.Register(entity);
            }
        }
    }

    public bool CanMove(Entity entity, Direction direction)
    {
        _nextCoordinate = _grid.WorldToCell(entity.transform.position) + DirectionHelper.ToVector3(direction);

        if (_nextCoordinate.x < 0 || _nextCoordinate.z < 0)
        {
            return false;
        }
        else if (_nextCoordinate.x >= _boardMaxWidth || _nextCoordinate.z >= _boardMaxHeight)
        {
            return false;
        }
        else
        {
            return _cells[_nextCoordinate.x, _nextCoordinate.z].IsCellAccessible(entity, direction);
        }
    }

    public void Register(Entity entity)
    {
        //keep track of all entities
        _entities.Add(entity);
        if (entity.GetComponent<Target>() != null) //counting number of targets to check when level finished
        {
            _numberOfTargets++;
        }
        //add entity to corresponding cell
        Cell cell = GetCellForEntity(entity);
        cell.AddEntity(entity);
    }

    public void Unregister(Entity entity)
    {
        _entities.Remove(entity);
        if (entity.GetComponent<Target>() != null) //discounting removed targets
        {
            _numberOfTargets--;
        }
        Cell cell = GetCellForEntity(entity);
        cell.RemoveEntity(entity);
    }

    public Cell GetCellForEntity(Entity entity)
    {
            var cellCoord = _grid.WorldToCell(entity.transform.position);
            var cell = _cells[cellCoord.x, cellCoord.z];

            return cell;
    }

    public void CheckForWin()
    {
        int _targetsFilled = 0;
        foreach (Entity target in _entities)
        {
            if(target.GetComponent<Target>() != null)
            {
                //counting filled targets
                if (target.GetComponent<Target>().CheckFill() == true)
                {
                    _targetsFilled++;
                }
            }
        }
        if (_targetsFilled == _numberOfTargets)
        {
            OnLevelFinished.Invoke();
        }
    }
}
