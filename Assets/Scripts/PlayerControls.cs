using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    private Move _moveComponent;
   
    void Awake()
    {
        _moveComponent = GetComponentInChildren<Move>();
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (!_moveComponent.IsMoving)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                _moveComponent.MoveTo(Direction.East);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                _moveComponent.MoveTo(Direction.West);
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                _moveComponent.MoveTo(Direction.North);
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                _moveComponent.MoveTo(Direction.South);
            }
        }
    }
}
