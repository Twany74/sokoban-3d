using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{   
    void Start()
    {
        GameBoard.Instance.Register(this);
    }

    protected void OnDestroy()
    {
        GameBoard.Instance.Unregister(this);
    }
}
