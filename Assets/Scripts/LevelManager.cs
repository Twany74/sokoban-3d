using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class LevelManager : MonoBehaviour
{
    public int currentLevel { get; private set; }
    public static LevelManager Instance { get; private set; }
    public int numbersOfLevels { get; private set; }
    [SerializeField] private int _setNumberOfLevels;
    public bool[] IsLevelAvailable { get; private set; }
    [SerializeField] private GameObject _player;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        LoadMainMenu();
    }

    // Start is called before the first frame update
    void Start()
    {
        Instance.IsLevelAvailable = new bool[_setNumberOfLevels];
        for (int i = 0; i < _setNumberOfLevels; i++)
        {
            Instance.IsLevelAvailable[i] = false;
        }
        Instance.currentLevel = 1;
        Instance.IsLevelAvailable[0] = true;
        numbersOfLevels = _setNumberOfLevels;
    }

    public static void LoadLevel(int lvl)
    {
        Instance._player.GetComponent<PlayerControls>().enabled = true;
        Instance.currentLevel = lvl;
        SceneManager.LoadScene("Level" + lvl);
        GameBoard.Instance.RestorePlayerLocation();
    }

    public static void LoadNextLevel()
    {
        LoadLevel(Instance.currentLevel);
    }

    public static void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void LevelWin()
    {
        Instance.currentLevel++;
        Instance.IsLevelAvailable[Instance.currentLevel - 1] = true;
    }
}
