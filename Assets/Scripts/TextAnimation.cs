using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextAnimation : MonoBehaviour
{
    [SerializeField] private TMP_Text _text;
    private float _animationLoopTimer;
    private float _maximum = 120.0f;
    private float _minimum = 70.0f;

    // Update is called once per frame
    void Update()
    {
        _text.fontSize = Mathf.Lerp(_minimum, _maximum, _animationLoopTimer);
        _animationLoopTimer += 2.0f * Time.deltaTime;
        if (_animationLoopTimer > 1.0f)
        {
            float temp = _maximum;
            _maximum = _minimum;
            _minimum = temp;
            _animationLoopTimer = 0.0f;
        }
    }
}
