using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Menu : MonoBehaviour
{
    void Awake()
    {
        DontDestroyOnLoad(this);
    }
    public void Reload()
    {
        LevelManager.LoadLevel(LevelManager.Instance.currentLevel);
    }

    public void NextLevel()
    {
        LevelManager.LoadNextLevel();
    }
}
