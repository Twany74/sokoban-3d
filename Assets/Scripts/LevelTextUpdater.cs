using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelTextUpdater : MonoBehaviour
{
    [SerializeField] private TMP_Text _text;
    void Update()
    {
        _text.text = "Level " + (LevelManager.Instance.currentLevel - 1);
    }
}
