using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell
{
    private List<Entity> _entities = new List<Entity>();

    public void AddEntity(Entity entity)
    {
        this._entities.Add(entity); 
    }

    public void RemoveEntity(Entity entity)
    {
        this._entities.Remove(entity);
    }

    public bool IsCellAccessible(Entity entityToMove, Direction direction)
    { 
        bool availability = false; //by default entityToMove can't move (in case of a hole for exemple), using this variable for when encounter an entity that can share its cell
        foreach (Entity entity in _entities) 
        {
            if (entity.GetComponent<Wall>() != null) //wall on cell -> entityToMove don't move
            {
                return false; //wall can't share its cell with an other entity so we return directly the accessibility value
            }
            if (entity.GetComponent<Target>() != null) //target on cell -> entityToMove could move if there is no blocking entity on the same cell
            {
                availability = true; //target can share its cell with an other entity so we temporary stock the accessibility value, until we checked all entities on the cell
            }
            if (entity.GetComponent<Crate>() != null) //crate on cell -> entityToMove can move if it doesn't be a crate itself (a crate can't push another crate)
            {
                if (entityToMove.GetComponent<PlayerControls>() != null) //check if entityToMove is a player, if yes, we call crate moving function, if not no one moves
                {
                    return entity.GetComponent<Crate>().MoveCrate(direction);
                }
                else
                {
                    return false; //the crate can't be moved so entityToMove also don't move
                }
            }
            if (entity.GetComponent<Floor>() != null) //floor on next cell -> entityToMove could move if there is no blocking entity on the same cell
            {
                availability = true; //floor can share its cell with others _entities so we temporary stock the accessibility value, until we checked all _entities on the cell
            }
        }
        return availability;
    }

    public Entity SearchForCratesAndPlayer()
    {
        foreach (Entity entity in _entities)
        {
            if (entity.GetComponent<PlayerControls>() != null)
            {
                return entity;
            }
            if (entity.GetComponent<Crate>() != null)
            {
                return entity;
            }
        }
        return null;
    }
}
