using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : MonoBehaviour
{
    private Move _moveComponent;

    void Awake()
    {
        _moveComponent = GetComponentInChildren<Translate>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool MoveCrate(Direction direction)
    {
        _moveComponent.MoveTo(direction);
        return GetComponentInChildren<Translate>().crateHasMoved; //return true if crate has moved so pushing entity can move too
    }
}
