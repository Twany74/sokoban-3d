using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roll : Move
{
    [SerializeField] private float _rollSpeedDegPerSec = 270.0f;
    private Entity _entity;

    void Awake()
    {
        _entity = GetComponent<Entity>();
    }

    public override void MoveTo(Direction direction)
    {
        if (IsMoving == false)
        {
            if (GameBoard.Instance.CanMove(_entity, direction))
            {
                GameBoard.Instance.Unregister(_entity);
                StartCoroutine(Rolling(DirectionHelper.ToVector3(direction), _rollSpeedDegPerSec));
            }
        }
    }

    public IEnumerator Rolling(Vector3 direction, float rotationSpeedInDegreePerSec)
    {
        float remainingAngle = 90.0f;
        Vector3 rotationCenter = transform.position + direction / 2 + Vector3.down / 2; //Set the rotation axis on moving side
        Vector3 rotationAxis = Vector3.Cross(Vector3.up, direction);
        IsMoving = true;
        while(remainingAngle > 0) 
        {
            float rotationAngle = Mathf.Min(Time.deltaTime * rotationSpeedInDegreePerSec, remainingAngle);
            transform.RotateAround(rotationCenter, rotationAxis, rotationAngle);
            remainingAngle -= rotationAngle;
            yield return null;
        }
        IsMoving = false;
        GameBoard.Instance.Register(_entity);
        Move.HasMoved();
    }
}
