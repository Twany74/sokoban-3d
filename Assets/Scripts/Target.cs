using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    private Cell _targetCell;

    public bool CheckFill()
    {
        _targetCell = GameBoard.Instance.GetCellForEntity(this.GetComponent<Entity>());
        Entity entityOnTarget = _targetCell.SearchForCratesAndPlayer();
        if (entityOnTarget != null)
        {
            if(entityOnTarget.GetComponentInChildren<Renderer>().material.color == this.GetComponent<Renderer>().material.color) //compare target color to the entity on it color
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
