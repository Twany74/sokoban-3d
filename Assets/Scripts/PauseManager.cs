using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PauseManager : MonoBehaviour
{
    public UnityEvent Pause;
    public UnityEvent UnPause;
    private bool _isGamePaused = false;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp("escape"))
        {
            if (_isGamePaused == false)
            {
                _isGamePaused = true;
                Pause.Invoke();
            }
            else
            {
                _isGamePaused = false;
                UnPause.Invoke();
            }
        }
    }
}
