using System;
using UnityEngine;

public enum Direction
{
    North,
    East,
    South,
    West
}

public class DirectionHelper
{
    public static Vector3Int ToVector3(Direction d)
    {
        switch (d)
        {
            case Direction.East:
                return new Vector3Int(1, 0, 0);
            case Direction.South:
                return new Vector3Int(0, 0, -1);
            case Direction.West:
                return new Vector3Int(-1, 0, 0);
            case Direction.North:
                return new Vector3Int(0, 0, 1);
            default:
                return Vector3Int.zero;
        }
    }
}